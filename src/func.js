const getSum = (str1, str2) => {

    if (typeof str1 != "string" || typeof str2 != "string") {
        return false;
    }

    if (str1 == "") {
        str1 = "0";
    }

    if (str2 == "") {
        str2 = "0";
    }

    if (str1.match(/\d/g) == null || str2.match(/\d/g) == null || str1.length != str1.match(/\d/g).length || str2.length != str2.match(/\d/g).length) {
        return false;
    }

    let result = "";
    let bigger = "";
    let smaller = "";

    if (str1.length > str2.length) {
        bigger = str1;
        smaller = str2;
    }
    else {
        bigger = str2;
        smaller = str1;
    }


    let i = smaller.length - 1;
    let j = bigger.length - 1;
    let inMind = 0;

    for (; j >= 0; i--, j--) {

        let res = Number.parseInt(bigger[j]) + (i < 0 ? 0 : Number.parseInt(smaller[i]));

        res += inMind;
        inMind = res >= 10 ? 1 : 0;
        res = inMind > 0 ? res - 10 : res;

        result += res;

    }

    result += inMind > 0 ? inMind : "";

    return result.split("").reverse().join("");

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

    let posts = listOfPosts.filter((x) => x.author == authorName);

    let postsWithComments = listOfPosts.filter((x) => x.hasOwnProperty("comments"));

    let comments = [];

    for (let post of postsWithComments) {
        for (let comment of post.comments) {
            comments.push(comment);
        }
    }

    comments = comments.filter((x) => x.author == authorName);

    return "Post:" + posts.length + ",comments:" + comments.length;

};

const tickets = (people) => {

    let wallet = { 25: 0, 50: 0, 100: 0 };

    let isPossible = true;

    for (let amount of people) {

        if (amount == 25) {
            wallet[25]++;
            continue;
        }

        if (amount == 50 && wallet[25] > 0) {
            wallet[50]++;
            wallet[50]--;
            continue;
        }

        if (amount == 100 && (wallet[50] > 0 && wallet[25] > 0)) {
            wallet[100]++;
            wallet[50]--;
            wallet[25]--;
            continue;
        }

        if (amount == 100 && (wallet[25] > 2)) {
            wallet[100]++;
            wallet[25] -= 3;
            continue;
        }

        isPossible = false;

    }

    return isPossible ? "YES" : "NO";

};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
